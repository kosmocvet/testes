<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\RequestBody;
use App\Controller\Driver\ChangeBalanceAmountController;
use App\Controller\Driver\DeleteCarController;
use App\Repository\DriverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DriverRepository::class)]
#[ApiResource(
    normalizationContext: [
        'groups' => ['driver:read'],
    ]
)]
#[Get]
#[GetCollection]
#[Post]
#[Put(
    uriTemplate: '/drivers/change-balance-amount',
    controller: ChangeBalanceAmountController::class,
    openapi: new Operation(
        summary: 'Change Balance Amount',
        description: 'Change Balance Amount',
        requestBody: new RequestBody(
            content: new \ArrayObject([
                'application/json' => [
                    'schema' => [
                        'type' => 'object',
                        'properties' => [
                            'driverId' => ['type' => 'integer'],
                            'amount' => ['type' => 'integer'],
                            'driverBalanceAction' => ['type' => 'string']
                        ]
                    ],
                    'example' => [
                        'driverId' => '1',
                        'amount' => '11',
                        'driverBalanceAction' => 'plus'
                    ]
                ]
            ])
        ),
    ),
    name: 'addBalanceAmount'
)]
#[Delete]
#[Delete(
    uriTemplate: '/drivers/delete-car',
    controller: DeleteCarController::class,
    openapi: new Operation(
        summary: 'Delete Car Controller',
        description: 'Delete Car Controller',
        requestBody: new RequestBody(
            content: new \ArrayObject([
                'application/json' => [
                    'schema' => [
                        'type' => 'object',
                        'properties' => [
                            'driverId' => ['type' => 'integer'],
                            'carId'    => ['type' => 'integer'],
                        ]
                    ],
                    'example' => [
                        'driverId' => '1',
                        'carId' => '1'
                    ]
                ]
            ])
        ),
    ),
    name: 'deleteCar'
)]
class Driver
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('driver:read')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('driver:read')]
    private ?string $fullName = null;

    #[ORM\Column(length: 20)]
    #[Groups('driver:read')]
    private ?string $phoneNumber = null;

    #[ORM\Column]
    #[Groups('driver:read')]
    private ?float $currentBalance = null;

    #[ORM\ManyToMany(targetEntity: Car::class, mappedBy: 'drivers')]
    #[Groups('driver:read')]
    private Collection $cars;

    #[ORM\OneToMany(targetEntity: FinanceHistory::class, mappedBy: 'driver')]
    #[Groups('driver:read')]
    private Collection $financeHistories;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
        $this->financeHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): static
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): static
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCurrentBalance(): ?float
    {
        return $this->currentBalance;
    }

    public function setCurrentBalance(float $currentBalance): static
    {
        $this->currentBalance = $currentBalance;

        return $this;
    }

    /**
     * @return Collection<int, Car>
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): static
    {
        if (!$this->cars->contains($car)) {
            $this->cars->add($car);
            $car->addDriver($this);
        }

        return $this;
    }

    public function removeCar(Car $car): static
    {
        if ($this->cars->removeElement($car)) {
            $car->removeDriver($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, FinanceHistory>
     */
    public function getFinanceHistories(): Collection
    {
        return $this->financeHistories;
    }

    public function addFinanceHistory(FinanceHistory $financeHistory): static
    {
        if (!$this->financeHistories->contains($financeHistory)) {
            $this->financeHistories->add($financeHistory);
            $financeHistory->setDriver($this);
        }

        return $this;
    }

    public function removeFinanceHistory(FinanceHistory $financeHistory): static
    {
        if ($this->financeHistories->removeElement($financeHistory)) {
            // set the owning side to null (unless already changed)
            if ($financeHistory->getDriver() === $this) {
                $financeHistory->setDriver(null);
            }
        }

        return $this;
    }
}
