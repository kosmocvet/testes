<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CarRepository::class)]
#[ApiResource]
#[Get]
#[GetCollection]
#[Post]
#[Delete]
class Car
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('driver:read')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('driver:read')]
    private ?string $model = null;

    #[ORM\Column(length: 255)]
    #[Groups('driver:read')]
    private ?string $goverNumber = null;

    #[ORM\Column(length: 255)]
    #[Groups('driver:read')]
    private ?string $color = null;

    #[ORM\ManyToMany(targetEntity: Driver::class, inversedBy: 'cars')]
    private Collection $drivers;

    public function __construct()
    {
        $this->drivers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): static
    {
        $this->model = $model;

        return $this;
    }

    public function getGoverNumber(): ?string
    {
        return $this->goverNumber;
    }

    public function setGoverNumber(string $goverNumber): static
    {
        $this->goverNumber = $goverNumber;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): static
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection<int, Driver>
     */
    public function getDrivers(): Collection
    {
        return $this->drivers;
    }

    public function addDriver(Driver $driver): static
    {
        if (!$this->drivers->contains($driver)) {
            $this->drivers->add($driver);
        }

        return $this;
    }

    public function removeDriver(Driver $driver): static
    {
        $this->drivers->removeElement($driver);

        return $this;
    }
}
