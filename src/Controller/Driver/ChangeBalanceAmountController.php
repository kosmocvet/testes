<?php

namespace App\Controller\Driver;

use App\Entity\Car;
use App\Entity\Driver;
use App\Entity\FinanceHistory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ChangeBalanceAmountController extends AbstractController
{

    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function __invoke(Request $request): Response
    {
        $em = $this->em;
        $data = json_decode($request->getContent(), true);

        if (isset($data['driverId'])) {
            $driverId = $data['driverId'];
        } else {
            throw new BadRequestHttpException('The "driverId" is required. Please, select a driverId .');
        }

        if (isset($data['amount'])) {
            $amount = $data['amount'];
            if(!is_numeric($amount) || $amount < 0){

                throw new BadRequestHttpException('The "amount" need be integer and > 0. Please, select a amount .');
            }
        } else {
            throw new BadRequestHttpException('The "amount" is required.');
        }

        if(isset($data['driverBalanceAction'])){
            $driverBalanceAction = $data['driverBalanceAction'];
            if($driverBalanceAction != 'plus' && $driverBalanceAction != 'minus'){
                throw new BadRequestHttpException('driverBalanceAction is not correct');
            }
        }

        $driver = $em->getRepository(Driver::class)->find($driverId);

        if (!$driver) {
            throw new BadRequestHttpException('Driver not found');
        }

        if($data['driverBalanceAction'] == 'plus'){
            $driver->setCurrentBalance($driver->getCurrentBalance() + $amount);
        } elseif($data['driverBalanceAction'] == 'minus') {
            $driver->setCurrentBalance($driver->getCurrentBalance() - $amount);
        }

        $financeHistory = new FinanceHistory();
        $financeHistory->setDriver($driver);
        $financeHistory->setAmount($amount);
        $financeHistory->setTime(new \DateTimeImmutable());
        $financeHistory->setAction($driverBalanceAction);
        $em->persist($financeHistory);
        $em->persist($driver);
        $em->flush();

        return new JsonResponse(
            [
                'success' => true,
                'message' => 'Amount added to driver balance'
            ],
            200
        );
    }
}


