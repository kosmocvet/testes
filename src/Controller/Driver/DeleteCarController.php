<?php

namespace App\Controller\Driver;

use App\Entity\Car;
use App\Entity\Driver;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DeleteCarController extends AbstractController
{

    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function __invoke(Request $request): Response
    {
        $em = $this->em;
        $data = json_decode($request->getContent(), true);

        if (isset($data['driverId'])) {
            $driverId = $data['driverId'];
        } else {
            throw new BadRequestHttpException('The "driverId" is required. Please, select a driverId .');
        }

        if (isset($data['carId'])) {
            $carId = $data['carId'];
        } else {
            throw new BadRequestHttpException('The "carId" is required. Please, select a carId .');
        }

        $driver = $em->getRepository(Driver::class)->find($driverId);
        $car = $em->getRepository(Car::class)->find($carId);

        if (!$driver) {
            throw new BadRequestHttpException('Driver not found');
        }

        if (!$car) {
            throw new BadRequestHttpException('Car not found');
        }

        $driver->removeCar($car);
        $em->persist($driver);
        $em->flush();

        return new JsonResponse(
            [
                'success' => true,
                'message' => 'Relation between driver and car has been deleted.'
            ],
            200
        );
    }
}


