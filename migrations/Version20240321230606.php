<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240321230606 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE car_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE driver_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE finance_history_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE car (id INT NOT NULL, model VARCHAR(255) NOT NULL, gover_number VARCHAR(255) NOT NULL, color VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE car_driver (car_id INT NOT NULL, driver_id INT NOT NULL, PRIMARY KEY(car_id, driver_id))');
        $this->addSql('CREATE INDEX IDX_90E902BC3C6F69F ON car_driver (car_id)');
        $this->addSql('CREATE INDEX IDX_90E902BC3423909 ON car_driver (driver_id)');
        $this->addSql('CREATE TABLE driver (id INT NOT NULL, full_name VARCHAR(255) NOT NULL, phone_number VARCHAR(20) NOT NULL, current_balance DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE finance_history (id INT NOT NULL, driver_id INT DEFAULT NULL, time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, action VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CA289CEFC3423909 ON finance_history (driver_id)');
        $this->addSql('ALTER TABLE car_driver ADD CONSTRAINT FK_90E902BC3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE car_driver ADD CONSTRAINT FK_90E902BC3423909 FOREIGN KEY (driver_id) REFERENCES driver (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE finance_history ADD CONSTRAINT FK_CA289CEFC3423909 FOREIGN KEY (driver_id) REFERENCES driver (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE car_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE driver_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE finance_history_id_seq CASCADE');
        $this->addSql('ALTER TABLE car_driver DROP CONSTRAINT FK_90E902BC3C6F69F');
        $this->addSql('ALTER TABLE car_driver DROP CONSTRAINT FK_90E902BC3423909');
        $this->addSql('ALTER TABLE finance_history DROP CONSTRAINT FK_CA289CEFC3423909');
        $this->addSql('DROP TABLE car');
        $this->addSql('DROP TABLE car_driver');
        $this->addSql('DROP TABLE driver');
        $this->addSql('DROP TABLE finance_history');
    }
}
